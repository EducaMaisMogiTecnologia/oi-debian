#!/bin/sh
(cd local-packages/oi-debian-default-settings; dpkg-buildpackage --no-sign)
(cd local-packages/oi-debian-freeze-users; dpkg-buildpackage --no-sign)
(cd local-packages/oi-debian-proinfo-multi-seat-settings; dpkg-buildpackage --no-sign)
build-simple-cdd --dvd --force-root --conf ./oi-debian.conf
